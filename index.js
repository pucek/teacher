const TelegramBot = require('node-telegram-bot-api');
const co = require('co');
const mongo = require('mongodb');
const Map = require('collections/fast-map');

const config = require('./configuration');
var db;

// Connect to the db
mongo.connect('mongodb://' + config.mongo.host + ':' + config.mongo.port + '/' + config.mongo.dbname, function(err, database) {
	if(err) { return console.dir(err); }
	db = database;
	getAuthorized();
//	var collection = db.collection('teacher');
//	collection.insertOne({
//		owner:   161853987,
//		foreign: "miód",
//		known:   "honey",
//		pinyin:  "yin yen",
//		succ:    0
//	});
});

var token = config.telegram.token;
// Setup polling way
var bot = new TelegramBot(token, {polling: true});

var global_state = new Map();

// pseudo database

var authorized = [];
var authorized_id = [];

var temp_foreign = new Map();
var temp_known   = new Map();
var temp_pinyin  = new Map();
var temp_succ    = new Map();

var exam_id      = new Map();
var exam_foreign = new Map();
var exam_known   = new Map();
var exam_pinyin  = new Map();
var exam_direct  = new Map(); // false - foreign to known, true - known to foreign
var exam_random  = new Map(); // false - least known, true - random index

// Matches non commands
bot.onText(/^(?!\/)(.+)/, function (msg, match) {
	return co(function* () {
		var fromId = msg.from.id;
		if (authorized_id.indexOf(fromId) === -1) {
			bot.sendMessage(fromId, config.bot.messages.unauthorized);
			return false;
		}
		var content = msg.text.toLowerCase();
		var state = global_state.get(fromId);
		switch (state) {
			case 1 :
				var direct = exam_direct.get(fromId);
				if (direct) {
					temp_known.set(fromId, content);
				} else {
					temp_foreign.set(fromId, content);
				}
				var correct = yield test(fromId);
				if (correct) {
					yield vocabularyUpdate(fromId);
					yield bot.sendMessage(fromId, 'bravo!');
					yield examine(bot, fromId);
				} else {
					yield bot.sendMessage(fromId, 'incorrect... try again...');
				}
				break;
			case 2 :
				temp_foreign.set(fromId, content);
				global_state.set(fromId, 3);
				yield bot.sendMessage(fromId, 'adding, now give your translation in ' + config.language.known);
				break;
			case 3 :
				temp_known.set(fromId, content);
				global_state.set(fromId, 4);
				yield bot.sendMessage(fromId, 'adding, now give pinyin');
				break;
			case 4 :
				temp_pinyin.set(fromId, content);
				yield newEntry(fromId);
				global_state.set(fromId, 1);
				yield bot.sendMessage(fromId, 'entry added to the database');
				break;
		}
	});
});

// Matches /add
bot.onText(/\/cancel/, function (msg, match) {
	var fromId = msg.from.id;
	if (authorized_id.indexOf(fromId) === -1) {
		bot.sendMessage(fromId, config.bot.messages.unauthorized);
		return false;
	}
	var state = global_state.get(fromId);
	if (state == 2 || state == 3 || state == 4) {
		global_state.set(fromId, 1);
		bot.sendMessage(fromId, 'cancelled adding');
	}
});

// Matches /add
bot.onText(/\/add/, function (msg, match) {
	var fromId = msg.from.id;
	if (authorized_id.indexOf(fromId) === -1) {
		bot.sendMessage(fromId, config.bot.messages.unauthorized);
		return false;
	}
	var state = global_state.get(fromId);
	if (state == 1) {
		global_state.set(fromId, 2);
		bot.sendMessage(fromId, 'adding, give representation in ' + config.language.foreign);
	}
});

// Matches /skip
bot.onText(/\/ask/, function (msg, match) {
	var fromId = msg.from.id;
	if (authorized_id.indexOf(fromId) === -1) {
		bot.sendMessage(fromId, config.bot.messages.unauthorized);
		return false;
	}
	global_state.set(fromId, 1);
	examine(bot, fromId);
});

// Matches /teach
bot.onText(/\/teach/, function (msg, match) {
	return co(function* () {
		var fromId = msg.from.id;
		if (authorized_id.indexOf(fromId) === -1) {
			bot.sendMessage(fromId, config.bot.messages.unauthorized);
			return false;
		}
		var exmf = exam_foreign.get(fromId);
		var exmk = exam_known.get(fromId);
		var exmp = exam_pinyin.get(fromId);
		bot.sendMessage(fromId, exmf + '\n' + exmk + '\n' + exmp + '\n');
	});
});

/*
// Matches /random
bot.onText(/\/random/, function (msg, match) {
	var fromId = msg.from.id;
	var state = global_state.get(fromId);
	exam_random = true;
	bot.sendMessage(fromId, 'Now I will take random vocabulary during practice');
});

// Matches /least
bot.onText(/\/least/, function (msg, match) {
	var fromId = msg.from.id;
	var state = global_state.get(fromId);
	exam_random = false;
	bot.sendMessage(fromId, 'Now I will take least known element during practice');
});
*/

// Matches /pinyin
bot.onText(/\/pinyin/, function (msg, match) {
	var fromId = msg.from.id;
	if (authorized_id.indexOf(fromId) === -1) {
		bot.sendMessage(fromId, config.bot.messages.unauthorized);
		return false;
	}
	var value_pinyin = exam_pinyin.get(fromId);
	bot.sendMessage(fromId, value_pinyin);
});

// Matches /skip
bot.onText(/\help/, function (msg, match) {
	var fromId = msg.from.id;
	if (authorized_id.indexOf(fromId) === -1) {
		bot.sendMessage(fromId, config.bot.messages.unauthorized);
		return false;
	}
	var content = "You can use following commands:\n- /help - to display this message\n- /ask - make me ask you a new question and discart actual one\n- /add - adding a new vocabulary to the database\n- /pinyin - if you have trouble typing or reading...\n\nI think that is all you need to know. Let's study!"
	bot.sendMessage(fromId, content);
});

function examine(bot, fromId) {
	return co(function* () {
//		if (exam_random) {
//			exam_id      = getRandomInt(0, succ.length - 1);
//		} else {
//			exam_id      = getIndexOfLowest();
//		}
		yield vocabularyGetRandom(fromId);
		rnd = getRandomInt(0, 1);
		if (rnd) {
			exam_direct.set(fromId, true);
			bot.sendMessage(fromId, 'how to say `' + exam_foreign.get(fromId) + '` in ' + config.language.known);
		} else {
			exam_direct.set(fromId, false);
			bot.sendMessage(fromId, 'how to say `' + exam_known.get(fromId) + '` in ' + config.language.foreign);
		}
	});
}

function test(fromId) {
	return co(function* () {
		var direct = exam_direct.get(fromId);
		var exmk = exam_known.get(fromId);
		var exmf = exam_foreign.get(fromId);
		var tmpk = temp_known.get(fromId);
		var tmpf = temp_foreign.get(fromId);
		if (direct) {
			return exmk == tmpk;
		} else {
			return exmf == tmpf;
		}
	});
}

function newEntry(fromId) {
	return co(function* () {
		vocabularyInsert(fromId);
		temp_foreign.set(fromId, '');
		temp_known.set(fromId,  '');
		temp_pinyin.set(fromId, '');
	});
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getIndexOfLowest() {
	var index = 0;
	var value = succ[index];
	var lowest = index;
	for(index = 0; index < succ.length; ++index) {
		if (succ[index] < value) {
			value = succ[index];
			lowest = index;
		}
	}
	return lowest;
}

// mongo related

// get authorized users info and chat ids

function getAuthorized() {
	var collection = db.collection('teacher_authorized');
	collection.find().toArray(function(err, results) {
        	authorized = results;
		// extract chat ids
		authorized.forEach(function(entry) {
			var id = entry.chatId;
			authorized_id.push(id);
			// initiate all data structures
			global_state.set(id, 1);
			temp_foreign.set(id, '');
			temp_known.set(id, '');
			temp_pinyin.set(id, '');
			temp_succ.set(id, 0);
			exam_id.set(id, '');
			exam_foreign.set(id, '');
			exam_known.set(id, '');
			exam_pinyin.set(id, '');
			exam_direct.set(id, false);
			exam_random.set(id, false);
		});
	});
}

// TODO get random vocabulary for user
// get random vocabulary entry among lowest

function vocabularyGetRandom(fromId) {
	return co(function* () {
		var collection = db.collection('teacher');
		var query = {
			owner: fromId
		};
		var count = yield collection.count(query);
		var random = getRandomInt(0, count - 1);
		var entry = yield collection.find(query).limit(-1).skip(random).next();
		// set the object
		temp_foreign.set(fromId, '');
		temp_known.set(fromId, '');
		temp_pinyin.set(fromId, '');
		temp_succ.set(fromId, entry['succ']);
		exam_id.set(fromId, entry['_id']);
		exam_foreign.set(fromId, entry['foreign']);
		exam_known.set(fromId, entry['known']);
		exam_pinyin.set(fromId, entry['pinyin']);
	});
}

// insert a new vocabulary entry

function vocabularyInsert(chatId) {
	return co(function* () {
		var collection = db.collection('teacher');
		collection.insertOne({
			owner:   chatId,
			foreign: temp_foreign.get(chatId),
			known:   temp_known.get(chatId),
			pinyin:  temp_pinyin.get(chatId),
			succ:    0
		});
	});
}

// update a new vocabulary entry
function vocabularyUpdate(fromId) {
	return co(function* () {
		var id = exam_id.get(fromId);
		var tmpscc = temp_succ.get(fromId) + 1;
		var tmpfrg = exam_foreign.get(fromId);
		var tmpknw = exam_known.get(fromId);
		var tmppin = exam_pinyin.get(fromId);
		var collection = db.collection('teacher');
		collection.update(
		{
			_id:     id
		},
		{
			owner:   fromId,
			foreign: tmpfrg,
			known:   tmpknw,
			pinyin:  tmppin,
			succ:    tmpscc
		});
	});
}

// initiated!

console.log('I am ' + config.bot.name + ' ready to teach ' + config.language.foreign);
